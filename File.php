<?php

class API {

    public function printFullName($fullName) {
        echo "Full Name: $fullName\n";
    }

    public function printHobbies($hobbies) {
        echo "Hobbies:\n";
        foreach ($hobbies as $hobby) {
            echo "\t$hobby\n";
        }
    }

    public function printPersonalInfo($personalInfo) {
        echo "Age: {$personalInfo->age}\n";
        echo "Email: {$personalInfo->email}\n";
        echo "Birthday: {$personalInfo->birthday}\n";
    }

}

// Sample usage of the API class
$api = new API();

$fullName = "Juan Dela Cruz";
$api->printFullName($fullName);

$hobbies = array("Playing Basketball", "Reading Books", "Listening Music");
$api->printHobbies($hobbies);

$personalInfo = (object) array(
    'age' => 20,
    'email' => 'juandelacruz@emailniya.com',
    'birthday' => 'January 1, 1987'
);
$api->printPersonalInfo($personalInfo);

?>
